/* eslint-disable no-eval */
/*!
 * Tobias Hofmann
 * Contact: https://www.itsfullofstars.de
 */
sap.ui.define([
	"./../library", 
	"sap/ui/core/Control", 
	"./BarcodeScannerRenderer",
	"../libs/quagga.min"
], function (library, Control, BarcodeScannerRenderer, barcode) {
	"use strict";

	/**
	 * Constructor for a new BarcodeScanner control.
	 *
	 * @param {string} [sId] id for the new control, generated automatically if no id is given
	 * @param {object} [mSettings] initial settings for the new control
	 *
	 * @class
	 * Some class description goes here.
	 * @extends sap.ui.core.Control
	 *
	 * @author Tobias Hofmann
	 * @version 1.0.0
	 *
	 * @constructor
	 * @public
	 * @alias de.itsfullofstars.barcodescanner.controls.BarcodeScanner
	 * @ui5-metamodel This control/element also will be described in the UI5 (legacy) designtime metamodel
	 */
	var BarcodeScanner = Control.extend("de.itsfullofstars.barcodescanner.controls.BarcodeScanner", {
		metadata: {
			library: "de.itsfullofstars.barcodescanner",
			properties: {
				"value": {
					type: "string",
					defaultValue: null
				},
				/**
				 * Show rectangular around the barcode in the image
				 */
				"showBox": {
					type: "boolean",
					defaultValue: true

				},
				"code": {
					type: "string",
					defaultValue: "ean_reader"
				},
				/**
				 * Framerate of video steam
				 */
				"frameRate": {
					type: "float",
					defaultValue: 1.0
				},
				/**
				 * Device ID of camera
				 */
				"deviceId": {
					type: "string"
				},
				/**
				 * Facing mode of camera. For smartphones and tablets with a front and rear camera
				 */
				"facingMode": {
					type: "string",
					defaultValue: "environment"
				},
				/**
				 * Color of the box drawn round barcode
				 */
				"boxLineColor": {
					type: "sap.ui.core.CSSColor",
					defaultValue: "green"
				},
				/**
				 * Color of the line inside the box around the bar code 
				 */
				"scanLineColor": {
					type: "sap.ui.core.CSSColor",
					defaultValue: "black"
				},
				/**
				 * Width of line for box around barcode and line inside box
				 */
				"lineWidth": {
					type: "sap.ui.model.type.Integer",
					defaultValue: 4
				},
				/**
				 * Width of the preview window in pixels
				 */
				"width": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "640px"
				},
				/**
				 * Height of the preview window in pixels
				 */
				"height": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "480px"
				},
				/**
				 * Width of the video capture window in pixels
				 */
				"videoWidth": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "1280px"
				},
                /**
                 * Height of the video capture window in pixels
                 */
                "videoHeight": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "960px"
	            }				
			},
			events: {
				/**
				 * Event is fired when the user clicks on the control.
				 */
				scanned: {
                    parameters: {
                        value: {
                            type: "string"
                        }
                    }
                }
			}
		},
		
		renderer: BarcodeScannerRenderer,
		
		/**
		 * Initialize data
		 */
		init: function () {
            this._displayingVideo = false;
        },
        
        
    	/**
		 * Stops the camera.
		 * No new images will be captured. Should be called by the UI5 app when the barcode code was successfully read.
		 * @public
		 */
		stopCamera: function(){
			var oVideo = this._getVideo();
			if (oVideo.srcObject){
				oVideo.srcObject.getVideoTracks().forEach( function(t){ t.stop(); });
			}
		},
		
		
        /**
         * Starts video capture from the camera
         * Started after the view is rendered. The user must give permission to access camera (one time).
         * Only works when the browser is supporting navigator.mediaDevices.getUserMedia
         * Only video will be capture, no audio. Video is set to environment camera
         * The camera will continuously capture through the requestAnimationFrame
         * @public
         */
        onAfterRendering: function (evt) {
        	try {
				var oVideo = this._getVideo();
				this._iFrame = 0;
			    if (oVideo && !this._displayingVideo) {
	                navigator.mediaDevices.getUserMedia({
	                    video: { 
							facingMode: this.getFacingMode(),
							frameRate: this.getFrameRate()
						 },
	                    audio: false
	                })
	                .then(function(stream) {
						oVideo.srcObject = stream;
						oVideo.setAttribute("playsinline", true);
						oVideo.play();
						// store DOM elements
						// used in further execution of program to show video images, capture image frame and draw a box around bar code
						this._oVideo = this._getVideo();
						this._canvas = this._getCanvas();
						requestAnimationFrame(this._startCapture.bind(this));						
	                }.bind(this))
	                .catch(function(err) {
	                    console.log("onAfterRendering: error");
	                    console.log(err);
	                });
	            }
        	} catch(err) {
        	}
		},
		

		/**
		 * Called when UI5 is cleaning up resources.
		 * Will end the capturing of images through the camera.
		 * @public
		 */
		onExit: function() {
			this.stopCamera();	
		},
		

		/**
		 * Starts capturing images from camera
		 * This is a continuous process. The user needs to give permission on the first access for the browser
		 * to access the camera.
		 * After a barcode code is found, the camera will continue to capture images. The developer of the UI5 app is 
		 * responsible to end the capturing process.
		 * 
		 * In case a video capturing is in process: 
		 * 1. capture a new frame
		 * 2. find a barcode and decode it
		 * 3. Inform canvas to draw a box in the next captured frame
		 * 4. Fire scanned event
		 * 
		 * @private
		 */
		_startCapture: function() {			
			if (this._oVideo) {
				// only read image when camera is ready, has an image and its a new image frame
				if (this._oVideo.readyState === this._oVideo.HAVE_ENOUGH_DATA && this._iFrame < this._oVideo.webkitDecodedFrameCount) {
					this._iFrame = this._oVideo.webkitDecodedFrameCount;
					//console.log("Frame: " + oVideo.webkitDecodedFrameCount);

					// check that video element exists
					var width = parseInt(this.getVideoWidth(), 10);
		            var height = parseInt(this.getVideoHeight(), 10);		            
			    	if (width && height) {
						// get image from camera as PNG
						var oImage = this._canvas.toDataURL("image/png", 1.0);

						// get canvas and draw camera image on it (this is what the user sees as camera video stream)
						var canvas2d = this._canvas.getContext('2d');
						canvas2d.drawImage(this._oVideo, 0, 0, width, height);
						
						// draw a box and line where the barcode was found. Only work after Quagga found the first barcode in the captured image
						if (this._result && this.getShowBox()) {
							this._highlightBarcodeCode(this._result);
							this._highlightBarcodeLine(this._result);
						}
						
						// decode barcode in image using Quagga
						Quagga.decodeSingle({
							decoder: {
								readers: [this.getCode()]
							},
							locator: {
								patchSize: 'medium',
								halfSample: false,
							},
							locate: true,
							src: oImage
						}, function(result){
							if (result) {
								// barcode found
								if(result.codeResult) {
									console.log("result", result.codeResult.code);
									
									if (result.box) {
										this._result = result;
									}
									// fire event
									this.fireScanned({
										value: result.codeResult.code
									});
								}
							 // no barcode found
							 } else {
								console.log("not detected");
								// do not continue to draw a box on the image if no barcode was found
								this._result = undefined;
							}
							// continue scanning
							requestAnimationFrame(this._startCapture.bind(this));
						}.bind(this));

		            }
				} else {
					// continue to capture image and barcode
					requestAnimationFrame(this._startCapture.bind(this));
				}
			}
	    },
		
		
	    /**
		 * Retrieves the canvas element from the DOM
		 * @private
		 */
    	_getVideo: function() {
    		if (this.getDomRef()) {
        		return this.getDomRef().lastElementChild;
        	} else {
        		return null;
        	}
    	},
		
		
    	/**
    	 * Retrieves the canvas element from the DOM
    	 * @private
    	 */
		_getCanvas: function() {
			var canvasElement = this.getDomRef().getElementsByTagName("canvas")[0];
	    	return canvasElement;
		},


		/**
		 * Retrieves the canvas 2d element from the DOM
		 */
		_getCanvas2d: function() {
			var canvasElement = this.getDomRef().getElementsByTagName("canvas")[0];
			var canvas = canvasElement.getContext('2d');
			return canvas;
		},
		

		/**
		 * Highlight the identified barcode code in the image
		 * Draws a line around the barcode code in the given color
		 * @private
		 */
		_highlightBarcodeCode: function(result) {
			var canvas = this._getCanvas2d();
			
			canvas.strokeStyle = this.getBoxLineColor();
			canvas.lineWidth = this.getLineWidth();
			canvas.beginPath();
			canvas.moveTo(result.box[0][0], result.box[0][1]);
			for (var i = 0; i < result.box.length; i++) {
				canvas.lineTo(result.box[i][0], result.box[i][1]);
			}
			canvas.closePath();
			canvas.stroke();
		},

		
		/**
		 * Draw a fake scanner line in the barcode box
		 * @param {} result 
		 */
		_highlightBarcodeLine: function(result) {
			var canvas = this._getCanvas2d();

			// draw red line in middle of barcode
			canvas.strokeStyle = this.getScanLineColor();
			canvas.lineWidth = this.getLineWidth();
			canvas.beginPath();
			canvas.moveTo(result.line[0].x, result.line[0].y);
			for (var i = 0; i < result.line.length; i++) {
				canvas.lineTo(result.line[i].x, result.line[i].y);
			}
			canvas.closePath();
			canvas.stroke();
		}
		
    });
	return BarcodeScanner;
}, /* bExport= */ true);