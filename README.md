# barcode scanner

Barcode scanner control for UI5. This is a custom UI5 control for a barcode scanner. It uses Quagga for barcode reading and supports 1d codes.